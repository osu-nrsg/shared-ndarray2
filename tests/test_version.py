from pathlib import Path

import tomli

import shared_ndarray2


def test_version():
    pyproj_path = Path(__file__).parent.parent / "pyproject.toml"
    with open(pyproj_path, "rb") as pyproj_f:
        config = tomli.load(pyproj_f)
    assert config["tool"]["poetry"]["version"] == shared_ndarray2.__version__
